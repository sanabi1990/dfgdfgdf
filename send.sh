#!/bin/bash
clear
echo "$(tput setaf 15)#################################################################################"
echo "$(tput setaf 15)##                                                                             ##"
echo "$(tput setaf 15)##                      LOCAL FILE SENDER USING SSH                            ##"
echo "$(tput setaf 15)##                                                                             ##"
echo "$(tput setaf 15)#################################################################################"
echo "                                            $(tput setaf 3)Created by Youssef Benadda (ybenadda)"


echo "$(tput setaf 6)👉  $(tput bold)Enter The Username$(tput setaf 2)"
read name
echo "$(tput setaf 6)👉  Enter The $(tput bold)IP address$(tput setaf 2)"
read ip
echo "$(tput setaf 6)📨  Drag $(tput bold)source path$(tput setaf 2)"
read path1
echo "$(tput setaf 6)📪  Enter $(tput bold)destination path$(tput setaf 2)"
read path2
echo "$(tput setaf 1)🔑  SSH RECEIVER PASSWORD"
scp -p 22000 -r ${path1} ${name}@${ip}:${path2}
echo "$(tput setaf 6)✅  done"
